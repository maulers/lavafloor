extends MeshInstance


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export(NodePath) var lavaFloor
export(NodePath) var viewPort

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func set_ballPostition(x, z):
	pass#print(self.get_translation())
	#self.set_translation(Vector3(x, self.get_translation().y, z))
	#self.position.x = x
	#self.position.z = z

func _physics_process(delta):
	if Input.is_action_pressed("w"):
		self.translation.z -= 0.5/2
	if Input.is_action_pressed("s"):
		self.translation.z += 0.5/2
	if Input.is_action_pressed("a"):
		self.translation.x -= 0.5/2
	if Input.is_action_pressed("d"):
		self.translation.x += 0.5/2
	
	#print(Vector3(self.get_translation().x, self.get_translation().y, self.get_translation().z))
	#get_node(str(world)).paint_uv(Vector3(self.get_translation().x, self.get_translation().y, self.get_translation().z), Vector3(0,1,0), Color("#ffff00"))
	var Coords = get_node(str(lavaFloor)).getUV(self.get_translation().x, (self.get_translation().z))
	get_node(str(viewPort)).set_SrpitePosition(Coords[0], Coords[1])

