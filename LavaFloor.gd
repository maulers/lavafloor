extends MeshInstance

var offsetX = 0
var offsetZ = 0
var offsetRatio = 0.5
var offsetMultiplier = 1

# Called when the node enters the scene tree for the first time.
func _ready():
	print("GLOBAL TRANFORM", self.get_global_transform().origin)
	calculate_offsets()
	#print(self.get_mesh().get_size())
	#It is neccessary for create a collider for the plane mesh
	self.create_trimesh_collision()
	pass # Replace with function body.


func calculate_offsets():
	var xFirst = self.get_global_transform().origin.x - self.get_mesh().get_size().x/2 
	#var xLast = self.get_global_transform().origin.x + self.get_mesh().get_size().x/2 
	var zFirst = self.get_global_transform().origin.z - self.get_mesh().get_size().y/2 
	#var zLast = self.get_global_transform().origin.z + self.get_mesh().get_size().y/2 
	
	self.offsetX = (-1 * xFirst) - self.get_mesh().get_size().x/2
	self.offsetZ = (-1 * zFirst) - self.get_mesh().get_size().y/2
	
func getUV(posX, posZ):
	var uTexCoord = ((posX + self.offsetX) / self.get_mesh().get_size().x) + self.offsetRatio
	var vTexCoord = ((posZ + self.offsetZ) / self.get_mesh().get_size().y) + self.offsetRatio
	return [uTexCoord, vTexCoord]
	#print("U: ", uTexCoord, "V: ", vTexCoord)
	
