extends Camera

export(NodePath) var planeViewPort
export(NodePath) var ballMesh
export(NodePath) var world

const ray_length = 1000

func _physics_process(delta):
	#get_uv(Vector3(0,30,0), Vector3(25,25,0), Vector3(0,25,25), get_node(str(ballMesh)).get_translation())
	if Input.is_action_just_pressed("fire"):
		var mouse = get_viewport().get_mouse_position()
		var from = self.project_ray_origin(mouse)
		var to = self.project_ray_normal(mouse)
		#$RayCast.look_at(Vector3(event.position.x,event.position.y,0),Vector3(0,1,0))
		#$RayCast.look_at(to,Vector3(1,1,1))
		#$RayCast.cast_to = from + to * ray_length
		#$RayCast.translation = from
		#$RayCast.rotate(Vector3(event.position.x, event.position.y, 0), 0)
		#print($RayCast.cast_to)
		#print($RayCast.is_colliding())
		#print($RayCast.get_collision_point())
		#if($RayCast.is_colliding()):
		#	print($RayCast.get_collider().get_parent().get_mesh())
			#get_node(str(planeViewPort)).set_SrpitePosition($RayCast.get_collision_point().x,$RayCast.get_collision_point().y)
			#get_uv(Vector3(0,30,0), Vector3(10,25,0), Vector3(0,25,10), $RayCast.get_collision_point())
		var space_state = get_world().direct_space_state
		var result = space_state.intersect_ray(from, to)
		#get_node(str(ballMesh)).set_ballPostition(result['position'].x, result['position'].z)
		print(result)
		#if !result.empty():
			#get_node(str(world)).paint_uv(result['position'], result['normal'], Color("#ffff00"))
		
		
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func get_uv(N, E1, E2, p):
	print(N.normalized())
	var u = E1.normalized().dot(p)
	var v = E2.normalized().dot(p)
	#print ("U coordinate: " , u)
	#print ("V coordinate: " , v)
	get_node(str(planeViewPort)).set_SrpitePosition(u, v)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func _input(event):
	# Mouse in viewport coordinates
	if event is InputEventMouseButton:
		pass#print("Mouse Click/Unclick at: ", event.position)
		
	
