shader_type spatial;

uniform sampler2D viewporttexture : hint_white;

uniform vec2 amplitude = vec2(0.2, 0.3);
uniform vec2 frequency = vec2(3.0, 2.5);
uniform vec2 time_factor = vec2(2.0, 3.0);

uniform sampler2D texturemap : hint_albedo;
uniform vec2 texture_scale =  vec2(8.0, 4.0);
uniform vec2 contact_scale =  vec2(8.0, 4.0);
uniform float contact_scaler = 3.0;

uniform sampler2D emissionmap : hint_white;

uniform sampler2D heightmap : hint_white;
uniform float height_range = 0.5;
uniform float height_offset = -0.65;

uniform sampler2D maskmap : hint_white;

uniform sampler2D uv_offset_texture : hint_black;
uniform vec2 uv_offset_scale = vec2(0.2, 0.2);
uniform float uv_offset_time_scale = 0.05;
uniform float uv_offset_time_amplitude = 0.2;

uniform sampler2D normalmap : hint_normal;

uniform float refraction = 0.05;

float height(vec2 pos, float time){
	return (amplitude.x * sin( pos.x * frequency.x + time * time_factor.x)) + (amplitude.y * sin(pos.y * frequency.y + time * time_factor.y));
}

void vertex(){
	
	float h = (texture(heightmap, UV * texture_scale).r + height_offset) * height_range;
	float vph = texture(viewporttexture, UV).r; //dont have to multiply with anything the UV e.g: with texture_scale beacuse we use one to one mapping with viewport. Viewport cover the entire plane
	//VERTEX.x += height(VERTEX.xz, TIME);
	VERTEX.y += height(VERTEX.xz, TIME) + h + height_offset;
	VERTEX.y += vph * contact_scaler;

	//5,3 5,2.6 0,0.4
	TANGENT = normalize(vec3(0.0, height(VERTEX.xz + vec2(0.0, 0.2), TIME) - height(VERTEX.xz + vec2(0.0, -0.2), TIME), 0.4));
	BINORMAL = normalize(vec3(0.4, height(VERTEX.xz + vec2(0.2, 0.0), TIME) - height(VERTEX.xz + vec2(-0.2, 0.0), TIME), 0.0));
	NORMAL = cross(TANGENT, BINORMAL);
}

void fragment(){
	vec2 base_uv_offset = UV * uv_offset_scale;
	base_uv_offset += TIME * uv_offset_time_scale;
	
	vec2 texture_based_offset = texture(uv_offset_texture, base_uv_offset).rg;
	texture_based_offset = texture_based_offset * 2.0 - 1.0;
	
	vec2 texture_uv = UV * texture_scale;
	texture_uv += uv_offset_time_amplitude * texture_based_offset;
	
	vec2 scaled_uv = UV * texture_scale;
	ALBEDO = texture(texturemap, scaled_uv).rgb;
	/*if(ALBEDO.r > 0.9 && ALBEDO.g > 0.9 && ALBEDO.b > 0.9){
		ALPHA = 1.0;
	}else{
		ALPHA = 0.95;
	}*/
	
	NORMALMAP = texture(normalmap, base_uv_offset).rgb;
	
	METALLIC = 0.35;
	EMISSION = texture(emissionmap, scaled_uv).rgb;
	ROUGHNESS = 0.5;
	
	vec3 ref_normal = normalize(mix(NORMAL, TANGENT * NORMALMAP.x + BINORMAL * NORMALMAP.y + NORMAL * NORMALMAP.z, NORMALMAP_DEPTH));
	vec2 ref_ofs = SCREEN_UV -ref_normal.xy * refraction;
	EMISSION += textureLod(SCREEN_TEXTURE, ref_ofs, ROUGHNESS * 8.0).rgb * (1.0 - ALPHA);
	
	ALBEDO *= ALPHA;
	ALPHA = 1.0;	
}	

